import 'dart:convert';

import 'package:http/http.dart' as http;

class HttpService {
  Future<String> getPost(String endpoint, String apikey, String spass) async {
    Codec<String, String> stringToBase64 = utf8.fuse(base64);
    String encoded = stringToBase64.encode(spass);

    Map data = {
      "api_key": apikey,
      "spass": encoded,
    };

    var body = json.encode(data);

    final Uri url = Uri.parse(endpoint);

    http.Response res = await http.post(
      url,
      body: body,
      headers: {"Content-Type": "application/json"},
    );

    String post = res.body;
    return post;
  }
}
