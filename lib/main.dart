import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'http_service.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<String> questions = [
    "Is the act in question the acquisition of some information?",
    "Is the act in question the installation or use of an electronic, mechanical, or other surveillance device?",
    "If the act in question was an acquisition, was it achieved through the use of an electronic, mechanical, or other surveillance device?",
    "Was the information acquired the contents of wire communication?",
    "Was the information acquired the contents of radio communication?",
    "Were the circumstances such that a person has a reasonable expectation of privacy and a warrant would be required for law enforcement purposes?",
    "Did the act in question occur in the United States?",
    "Is this act permitted under section 2511(2)(i) of title 18?",
    "Was the act intentional?",
    "Was the purpose of the act to monitor or acquire information?",
    "Was the sender a known US person?",
    "Was the sender targeted by this act?",
    "Was the sender of the information in the United States?",
    "Did the sender of the information consent to the act in question?",
    "Was the intended recipient of the information a known US person?",
    "Was the intended recipient of the information targeted by this act?",
    "Did the intended recipient of the information consent to the act in question?",
    "Were all intended recipient(s) located within the United States?",
  ];
  List<bool> questionResults = [for (int i = 0; i < 18; i += 1) false];

  final endpointController = TextEditingController();
  final apikeyController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    endpointController.dispose();
    apikeyController.dispose();
    super.dispose();
  }

  final HttpService httpService = HttpService();
  Future<String>? _result;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.grey[300],
        body: SafeArea(
          child: Center(
            child: SingleChildScrollView(
              child: mainColumn(),
            ),
          ),
        ),
      ),
    );
  }

  Column mainColumn() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Icon(
          Icons.lock_person_outlined,
          size: 100,
        ),
        const SizedBox(height: 20),
        Text(
          "SPASS FISA",
          style: GoogleFonts.firaSans(
            fontSize: 36,
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 10),
        Text(
          "Please supply the following:",
          style: GoogleFonts.firaSans(
            fontSize: 24,
          ),
        ),
        const SizedBox(height: 30),
        Text(
          "Endpoint Config:",
          style: GoogleFonts.firaSans(
            fontSize: 18,
          ),
        ),
        const SizedBox(height: 10),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 30.0),
          padding: const EdgeInsets.only(left: 8.0),
          decoration: BoxDecoration(
              color: Colors.grey[200],
              border: Border.all(color: Colors.white),
              borderRadius: BorderRadius.circular(12)),
          child: TextField(
            controller: endpointController,
            decoration: const InputDecoration(
              border: InputBorder.none,
              hintText: "Endpoint URI",
            ),
          ),
        ),
        const SizedBox(height: 10),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 30.0),
          padding: const EdgeInsets.only(left: 8.0),
          decoration: BoxDecoration(
              color: Colors.grey[200],
              border: Border.all(color: Colors.white),
              borderRadius: BorderRadius.circular(12)),
          child: TextField(
            controller: apikeyController,
            decoration: const InputDecoration(
              border: InputBorder.none,
              hintText: "API Key",
            ),
          ),
        ),
        const SizedBox(height: 10),
        Text(
          "Options:",
          style: GoogleFonts.firaSans(
            fontSize: 18,
          ),
        ),
        const SizedBox(height: 10),
        uiCheckbox(0),
        const SizedBox(height: 10),
        uiCheckbox(1),
        const SizedBox(height: 10),
        uiCheckbox(2),
        const SizedBox(height: 10),
        uiCheckbox(3),
        const SizedBox(height: 10),
        uiCheckbox(4),
        const SizedBox(height: 10),
        uiCheckbox(5),
        const SizedBox(height: 10),
        uiCheckbox(6),
        const SizedBox(height: 10),
        uiCheckbox(7),
        const SizedBox(height: 10),
        uiCheckbox(8),
        const SizedBox(height: 10),
        uiCheckbox(9),
        const SizedBox(height: 10),
        uiCheckbox(10),
        const SizedBox(height: 10),
        uiCheckbox(11),
        const SizedBox(height: 10),
        uiCheckbox(12),
        const SizedBox(height: 10),
        uiCheckbox(13),
        const SizedBox(height: 10),
        uiCheckbox(14),
        const SizedBox(height: 10),
        uiCheckbox(15),
        const SizedBox(height: 10),
        uiCheckbox(16),
        const SizedBox(height: 10),
        uiCheckbox(17),
        const SizedBox(height: 10),
        Container(
          alignment: Alignment.center,
          padding: const EdgeInsets.all(8.0),
          child: (_result == null) ? buildColumn() : buildFutureBuilder(),
        ),
        const SizedBox(height: 15),
        Text(
          "Created By:",
          style: GoogleFonts.firaSans(fontSize: 14),
        ),
        const SizedBox(height: 5),
        Text(
          "Cody Neiman, Matteo Carrabba, Matthew Cline",
          style: GoogleFonts.firaSans(fontSize: 14),
        ),
      ],
    );
  }

  Column buildColumn() {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            setState(() {
              _result = httpService.getPost(endpointController.text,
                  apikeyController.text, spassComplete(questionResults));
            });
          },
          child: Container(
            margin: const EdgeInsets.symmetric(horizontal: 30.0),
            padding: const EdgeInsets.all(20),
            decoration: BoxDecoration(
                color: Colors.deepPurple,
                borderRadius: BorderRadius.circular(12)),
            child: const Center(
              child: Text(
                "Submit",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
        ),
      ],
    );
  }

  FutureBuilder<String> buildFutureBuilder() {
    return FutureBuilder<String>(
      future: _result,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data!.contains("Proof found.")) {
            return Column(
              children: [
                Text(snapshot.data!),
                proofResult(true),
              ],
            );
          } else {
            return Column(
              children: [
                Text(snapshot.data!),
                proofResult(false),
              ],
            );
          }
        } else if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }

        return const CircularProgressIndicator();
      },
    );
  }

  Container uiCheckbox(int question) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 30.0),
      padding: const EdgeInsets.only(left: 8.0),
      decoration: BoxDecoration(
          color: Colors.grey[200],
          border: Border.all(color: Colors.white),
          borderRadius: BorderRadius.circular(12)),
      child: CheckboxListTile(
        title: Text(questions[question]),
        value: questionResults[question],
        onChanged: (bool? value) {
          setState(() {
            questionResults[question] = value!;
          });
        },
      ),
    );
  }

  Container proofResult(bool result) {
    if (result) {
      return proofSuccess();
    } else {
      return proofFail();
    }
  }

  Container proofSuccess() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 200.0),
      padding: const EdgeInsets.all(20),
      decoration: BoxDecoration(
          color: Colors.green[400], borderRadius: BorderRadius.circular(12)),
      child: const Center(
        child: Text(
          "Proof Found",
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  Container proofFail() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 200.0),
      padding: const EdgeInsets.all(20),
      decoration: BoxDecoration(
          color: Colors.red[400], borderRadius: BorderRadius.circular(12)),
      child: const Center(
        child: Text(
          "Proof Not Found",
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  String spassComplete(List<bool> options) {
    String spassBase = """
begin_problem(FISA).

list_of_descriptions.
name({*CPSC 415 FISA Project*}).
author({*Matthew Cline, Matteo Carrabba, and Cody Neiman*}).
status(unsatisfiable).
description({*Logical model of the definition of Electronic Surveillance under the Foreign Intelligence Surveillance Act, as given in 50 USC 1801(f)*}).
end_of_list.

list_of_symbols.
  functions[(act, 0), (sender, 0), (receiver, 0), (message, 0), (situation, 0), (state, 0), (device, 0)].
  predicates[(isAcquisition, 1), (forSurveillance, 1), (isIntentional, 1), (isInstallation, 1), (toMonitor, 1), (byElecMechOther, 1), (isUSPerson, 1), (wireComms, 1), (radioComms, 1), (isKnown, 1), (inUS, 1), (targeted, 1), (reasonableExpectation, 1), (perm25, 1), (notConsent, 1), (isSurveillance1, 1), (isSurveillance2, 1), (isSurveillance3, 1), (isSurveillance4, 1)].
end_of_list.

list_of_formulae(axioms).

formula(implies(and(isAcquisition(act), byElecMechOther(act), or(radioComms(message), wireComms(message)), or(and(isUSPerson(sender), isKnown(sender), inUS(sender), targeted(sender)), and(isUSPerson(receiver), isKnown(receiver), inUS(receiver), targeted(receiver))), reasonableExpectation(situation)), isSurveillance1(act))).

formula(implies(and(isAcquisition(act), byElecMechOther(act), wireComms(message), notConsent(sender), notConsent(receiver), inUS(act), or(inUS(sender), inUS(receiver)), not(perm25(act))), isSurveillance2(act))).

formula(implies(and(isAcquisition(act), isIntentional(act), byElecMechOther(act), radioComms(message), reasonableExpectation(situation), and(inUS(sender), inUS(receiver))), isSurveillance3(act))).

formula(implies(and(isInstallation(act), byElecMechOther(act), inUS(act), toMonitor(message), not(radioComms(message)), not(wireComms(message)), reasonableExpectation(situation)), isSurveillance4(act))).

% Options
""";

    String spassOptions = "";

    if (options[0]) {
      spassOptions += """

% ${questions[0]}
formula(isAcquisition(act)).
""";
    }

    if (options[1]) {
      spassOptions += """

% ${questions[1]}
formula(isInstallation(act)).
formula(byElecMechOther(act)).
""";
    }

    if (options[2]) {
      spassOptions += """

% ${questions[2]}
formula(byElecMechOther(act)).
""";
    }

    if (options[3]) {
      spassOptions += """

% ${questions[3]}
formula(wireComms(message)).
""";
    } else {
      spassOptions += """

% ${questions[3]}
formula(not(wireComms(message))).
""";
    }

    if (options[4]) {
      spassOptions += """

% ${questions[4]}
formula(radioComms(message)).
""";
    } else {
      spassOptions += """

% ${questions[4]}
formula(not(radioComms(message))).
""";
    }

    if (options[5]) {
      spassOptions += """

% ${questions[5]}
formula(reasonableExpectation(situation)).
""";
    }

    if (options[6]) {
      spassOptions += """

% ${questions[6]}
formula(inUS(act)).
""";
    }

    if (!options[7]) {
      spassOptions += """

% ${questions[7]}
formula(not(perm25(act))).
""";
    }

    if (options[8]) {
      spassOptions += """

% ${questions[8]}
formula(isIntentional(act)).
""";
    }

    if (options[9]) {
      spassOptions += """

% ${questions[9]}
formula(toMonitor(message)).
""";
    }

    if (options[10]) {
      spassOptions += """

% ${questions[10]}
formula(isKnown(sender)).
formula(isUSPerson(sender)).
""";
    }

    if (options[11]) {
      spassOptions += """

% ${questions[11]}
formula(targeted(sender)).
""";
    }

    if (options[12]) {
      spassOptions += """

% ${questions[12]}
formula(inUS(sender)).
""";
    }

    if (!options[13]) {
      spassOptions += """

% ${questions[13]}
formula(notConsent(sender)).
""";
    }

    if (options[14]) {
      spassOptions += """

% ${questions[14]}
formula(isKnown(receiver)).
formula(isUSPerson(receiver)).
""";
    }

    if (options[15]) {
      spassOptions += """

% ${questions[15]}
formula(targeted(receiver)).
""";
    }

    if (!options[16]) {
      spassOptions += """

% ${questions[16]}
formula(notConsent(receiver)).
""";
    }

    if (options[17]) {
      spassOptions += """

% ${questions[17]}
formula(inUS(receiver)).
""";
    }

    String spassSuffix = """
end_of_list.

list_of_formulae(conjectures).

formula(or(isSurveillance1(act), isSurveillance2(act), isSurveillance3(act), isSurveillance4(act))).

end_of_list.
% this is a comment. We like comments ;-)
list_of_settings(SPASS).
{*
set_flag(PGiven,1).
set_flag(PProblem,0).
*}
end_of_list.

end_problem.

""";

    String spassComplete = spassBase + spassOptions + spassSuffix;

    print(spassComplete);

    return spassComplete;
  }
}
